require "mandrill"

class PaymentNotificationMailer < ActionMailer::Base
	default(
	from: 'coach_justin@rushnm.com',
	reply_to: 'coach_justin@rushnm.com'
	)
	def notification_email(cur_player, cur_tryout, t_num)
		@cur_player = cur_player
		@cur_tryout = cur_tryout
		@t_num = t_num
		mail(to: @cur_player.email, subject: 'Your payment for Rush tryouts') do |format|
			format.html # renders notification_email.html.erb for body of email
			format.pdf do
				attachments['tryout.pdf'] = WickedPdf.new.pdf_from_string(
				render_to_string(:pdf => 'tryout', :template =>
				'payment_notification_mailer/notification_email.pdf.erb')
				)
			end
		end
	end
end
