class Profile < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable#, :confirmable
 #has_one :guardian


        after_create :create_player
        def create_player
          @user = Profile.find(id)
          @user.type="Player"
          @user.save

        end

        # This method associates the attribute ":avatar" with a file attachment
          has_attached_file :photo, :path => "/:attachment/:style/:filename",styles: {
            thumb: '100x100>',
            square: '200x200#',
            medium: '300x300>'
          }, :default_url => "https://s3.amazonaws.com/alan.idi/photos/medium/missing.png"

          # Validate the attached image is image/jpg, image/png, etc
          validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/

end
