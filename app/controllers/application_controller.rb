class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :authenticate_profile!

  before_filter :configure_permitted_parameters, if: :devise_controller?

   protected

       def configure_permitted_parameters
           devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:email, :password, :first_name, :last_name, :phone, :gender, :dob, :address1, :city, :state, :zip, :experience, :photo ) }
           devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:email, :password, :current_password,:first_name, :last_name, :phone, :gender, :dob, :address1, :city, :state, :zip, :experience, :photo ) }
       end

end
