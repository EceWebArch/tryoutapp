class DropUnusedTables < ActiveRecord::Migration
  def change
    remove_foreign_key "guardians", "users"


    # or in the upcoming native support in Rails 4.2
    # add_foreign_key :posts, :users, on_delete: :cascade
  end




end
